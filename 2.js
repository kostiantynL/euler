const isEven = n => n % 2 === 0
const filterEven = arr => arr.filter(isEven)
const sumArrayItems = arr => arr.reduce((summ, i) => summ + i, 0)
const setFibonacciArray = limit => {
  const fibonacciNumberArray = []
  const fibIter = (acc, a, b) => {
    fibonacciNumberArray.push(a)

    if(limit > b) {
      fibIter(acc + 1, b, a + b)
    }
  }
  fibIter(0, 1, 1)

  return fibonacciNumberArray
}

const getResult = limit => sumArrayItems(filterEven(setFibonacciArray(limit)))

console.log(getResult(4000000))
