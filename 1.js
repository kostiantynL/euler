const sumOfMult = (n, counter = 0) => {
  for(let i=0; i < n; i++) {
    if(i % 3 === 0 || i % 5 === 0) {
      counter = counter + i
    }
  }

  return counter
}

console.log(sumOfMult(1000))
