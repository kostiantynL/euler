const findSmallestDivisor = (n, testDivisor) => {
  if (testDivisor ** 2 > n) { return n }
  if (n % testDivisor === 0) { return testDivisor }
  return findSmallestDivisor(n, testDivisor+1)
}
const smallestDisisor = n => findSmallestDivisor(n, 2)
const isPrime = n => n === smallestDisisor(n)

const findLargestPrimeFacror = n => isPrime(n) ? n : findLargestPrimeFacror(n - 1)
const largestPrimeFactor = n => findLargestPrimeFacror(n / 2)

console.log(largestPrimeFactor(600851475143))
