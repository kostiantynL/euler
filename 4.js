const isPalindrome = n => `${n}` === `${n}`.split('').reverse().join('')

const findPalindrome = () => {
  let result = 0
  for(let i=100; i<1000; i++) {
    for(let j=100; j<1000; j++) {
      const product = j * i

      if(isPalindrome(product) && product > result) {
        result = product
      }
    }
  }

  return result
}
